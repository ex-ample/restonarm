package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/entah/fngo"
)

func logWhenError(err error) {
	if err != nil {
		log.Println(err)
	}
}

func responseTempl(w http.ResponseWriter, code int, status string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, wErr := w.Write([]byte(fmt.Sprintf(`{"status": "%s"}`, status)))
	logWhenError(wErr)
}

func health(w http.ResponseWriter, r *http.Request) {
	responseTempl(w, 200, "ok")
}

func factorial(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	buf, readErr := ioutil.ReadAll(r.Body)
	if readErr != nil {
		logWhenError(readErr)
		responseTempl(w, 300, "bad request")
		return
	}

	payloadBody := struct {
		Number uint64 `json:"number"`
	}{}

	if jsonErr := json.Unmarshal(buf, &payloadBody); jsonErr != nil {
		logWhenError(jsonErr)
		responseTempl(w, 300, "bad request")
		return
	}

	result := big.NewInt(1)

	for ; payloadBody.Number > 0; payloadBody.Number-- {
		result = result.Mul(result, big.NewInt(int64(payloadBody.Number)))
	}

	w.WriteHeader(200)
	_, wErr := w.Write([]byte(fmt.Sprintf(`{"result": %v}`, result)))
	logWhenError(wErr)
}

func notFound(w http.ResponseWriter, r *http.Request) {
	responseTempl(w, 404, "not found")
}

type RedisTrx struct {
	OP   string          `json:"op"`
	TTL  int             `json:"ttl"`
	KEY  string          `json:"key"`
	DATA json.RawMessage `json:"data"`
}

func redisOp(ctx context.Context, req RedisTrx) (RedisTrx, error) {
	switch req.OP {
	case "SET":
		putErr := redisClient.Set(ctx, req.KEY, []byte(req.DATA), time.Second*time.Duration(req.TTL)).Err()
		logWhenError(putErr)
		return req, putErr

	case "DEL":
		getRes, getErr := redisOp(ctx, RedisTrx{OP: "GET", KEY: req.KEY})
		if getErr != nil {
			return getRes, getErr
		}

		delErr := redisClient.Del(ctx, req.KEY).Err()
		logWhenError(delErr)
		return getRes, delErr

	case "GET":
		getRes := redisClient.Get(ctx, req.KEY)
		if getRes.Err() != nil {
			logWhenError(getRes.Err())
			return req, getRes.Err()
		}

		buf, byteErr := getRes.Bytes()
		if byteErr != nil {
			logWhenError(getRes.Err())
			return req, byteErr
		}

		req.DATA = buf
		return req, nil

	default:
		return req, fmt.Errorf("operation %s not supported", req.OP)
	}
}

func redisOps(w http.ResponseWriter, r *http.Request) {
	buf, readErr := ioutil.ReadAll(r.Body)

	if readErr != nil {
		logWhenError(readErr)
		responseTempl(w, 300, "bad request")
		return
	}

	requestBody := struct {
		OP   string          `json:"op"`
		TTL  int             `json:"ttl"`
		KEY  string          `json:"key"`
		DATA json.RawMessage `json:"data"`
	}{}

	jsonErr := json.Unmarshal(buf, &requestBody)

	if jsonErr != nil {
		logWhenError(jsonErr)
		responseTempl(w, 300, "bad request")
		return
	}

	redisCtx, redisCancel := context.WithTimeout(context.Background(), time.Second*30)
	defer redisCancel()
	res, err := redisOp(redisCtx, requestBody)
	if err != nil {
		logWhenError(err)
		responseTempl(w, 500, "cannot processing request")
		return
	}

	jsonBuf, jsonMarshErr := json.Marshal(&res)
	if jsonMarshErr != nil {
		logWhenError(jsonMarshErr)
		responseTempl(w, 500, "cannot processing request")
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	w.Write(jsonBuf)
}

var stringOr func(str ...string) string
var redisClient *redis.Client

func init() {
	fngo.BindOrFunc(&stringOr)
	redisClient = redis.NewClient(&redis.Options{
		Addr: stringOr(os.Getenv("REDIS_SERVER"), "localhost:6379"),
	})
}

func main() {

	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path
		method := r.Method

		switch {
		case path == "/health" && method == http.MethodGet:
			health(w, r)
		case path == "/factorial" && method == http.MethodPost:
			factorial(w, r)
		case path == "/redis" && method == http.MethodPost:
			redisOps(w, r)
		default:
			notFound(w, r)
		}
	})

	server := &http.Server{
		Addr:    ":9090",
		Handler: mux,
	}

	go func(s *http.Server) {
		log.Printf("server listen on addr %v", s.Addr)
		s.ListenAndServe()
	}(server)

	quitChan := make(chan os.Signal, 1)
	signal.Notify(quitChan, os.Interrupt, os.Kill)
	sig := <-quitChan

	log.Printf("\ngot signal %v, exiting", sig)

	timeoutCtx, timeoutCancel := context.WithTimeout(context.Background(), time.Second*10)
	defer timeoutCancel()

	if quitErr := server.Shutdown(timeoutCtx); quitErr != nil {
		log.Panic(quitErr)
	}
}
