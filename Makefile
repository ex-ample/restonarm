
clean:
	rm -rf restonarm-linux_arm64 restonarm-linux_amd64 restonarm-darwin_amd64

build-arm:
	@rm -rf restonarm-linux_arm64
	CGO_ENABLED=0 GOARCH=arm64 GOOS=linux go build -o restonarm-linux_arm64
build-linux:
	rm -rf restonarm-linux_amd64
	CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -o restonarm-linux_amd64
build-darwin:
	rm -rf restonarm-darwin_amd64
	CGO_ENABLED=0 GOARCH=amd64 GOOS=darwin go build -o restonarm-darwin_amd64
local-deps:
	@hash docker &> /dev/null &&\
	docker info &> /dev/null &&\
	docker run \
	-e POSTGRES_PASSWORD=password \
	-e POSTGRES_USER=user \
	-e POSTGRES_DB=pgdb \
	-e POSTGRES_AUTH_METHOD=trust \
	--name dev-pgsql --rm -d --network host "postgres:alpine" &&\
	docker run \
	--name dev-redis --rm -d --network host "redis:alpine3.11"||\
	echo "Maybe you haven't install docker for mac, docker daemon not starting, or target container was running?"
local-teardown:
	@hash docker &> /dev/null &&\
	docker info &> /dev/null &&\
	docker stop dev-pgsql && docker stop dev-redis ||\
	echo "Maybe you haven't install docker for mac, docker daemon not starting, or target container wasn't running yet?"
test-android: build-arm
	@adb devices
	@adb push restonarm-linux_arm64 /data/local/tmp/restonarm
	@adb shell "chmod 755 /data/local/tmp/restonarm"
	@echo "device IP => $(shell adb shell ifconfig wlan0 | grep 'inet\s' | cut -d ":" -f 2 | cut -d " " -f 1)"
