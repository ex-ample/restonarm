module gitlab.com/ex-ample/restonarm

go 1.16

require (
	github.com/go-redis/redis/v8 v8.11.5
	gitlab.com/entah/fngo v0.0.1
)
